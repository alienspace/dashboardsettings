import React, { Component } from 'react';
import {
  Col, Button,
  Alert, Image, Tabs, Tab,
  Modal,
} from 'react-bootstrap';
import { browserHistory, Link, } from 'react-router';
import firebase from 'firebase';
import axios from 'axios';
import slug from 'slug';
import Dropzone from 'react-dropzone';
// import TinyMCE from 'react-tinymce';

/** COMPONENTS CUSTOM **/
import { APP_CONFIG } from './../../../boot.config';
import { TOOLS } from './../../../util/tools.global';
// import { MasterForm } from '../../Form/MasterForm';
// import { Input } from '../../Form/Input';
export class Settings extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading : true,
      showModal : false,
      confirmDelete : false,
      action : this.props.action,
      apps : [],
      packages: this.props.packages,
      files: [],

      uid : this.props.currentUser.uid,
      appID : document.querySelector('[data-js="root"]').dataset.appid || null,
      company_name : APP_CONFIG.PROJECT_NAME,
      company_slogan_name : APP_CONFIG.PROJECT_SLOGAN,
      company_razao_social : null,
      company_fantasy_name : null,
      company_cnpj : null,
      company_ie : null,
      company_address_zipcode : null,
      company_address : null,
      company_address_number : null,
      company_address_comp : null,
      company_address_bairro : null,
      company_address_city : null,
      company_address_state : null,
      company_email : null,
      company_telephone : null,
      company_telephone2 : null,
      company_telephone_whatsapp : null,
      company_skype : null,

      company_facebook : null,
      company_instagram : null,
      company_youtube : null,
      company_twitter : null,
      company_pinterest : null,
      company_linkedin : null,
      company_blog : null,

      logoURL: this.props.logoURL || null,
      user_name : null,
      status : true,

      status_response : null,
      message_response : null
    }

    this.checkStatusResponse = this.checkStatusResponse.bind(this);
    this.checkStates = this.checkStates.bind(this);
    this.resetCurrentState = this.resetCurrentState.bind(this);
    this.renderModal = this.renderModal.bind(this);
    this.requiredInput = TOOLS.requiredInput.bind(this);

    /*** FUNCTIONS AUXILIARES DO FORM ***/
    this.onLoadForm = this.onLoadForm.bind(this);
    this.onSubmitForm = this.onSubmitForm.bind(this);
    this.onUpdateForm = this.onUpdateForm.bind(this);
    this.onChangeForm = this.onChangeForm.bind(this);
    this._onChangeCompanyName = this._onChangeCompanyName.bind(this);
    this._onChangeSloganCompanyName = this._onChangeSloganCompanyName.bind(this);
    this._onChangeCompanyZipcode = this._onChangeCompanyZipcode.bind(this);
    this._onChangeUserName = this._onChangeUserName.bind(this);
    this.onChangeTiny = this.onChangeTiny.bind(this);

    /*** FUNCTIONS AUXILIARES DAS TABS **/
    this.changePackageDashboard = this.changePackageDashboard.bind(this);
    this.changePackageShop = this.changePackageShop.bind(this);
    this.changePackageSite = this.changePackageSite.bind(this);

    this.onDropFile = this.onDropFile.bind(this);
    this.onDropFileClick = this.onDropFileClick.bind(this);
    this.runUploadFiles = this.runUploadFiles.bind(this);

    /*** RENDER FUNCTIONS - TABS **/
    this.tabGeral = this.tabGeral.bind(this);
    this.tabSocial = this.tabSocial.bind(this);
    this.tabLayout = this.tabLayout.bind(this);
    this.tabModules = this.tabModules.bind(this);
    this.tabExtras = this.tabExtras.bind(this);

    /*** RENDER FUNCTIONS - ACTIONS ***/
    this.actionNull = this.actionNull.bind(this);
    this.renderAction = this.renderAction.bind(this);
  }

  componentWillMount(){
    // this.getPages();
    console.log('componentWillMount');
    if(this.props.action === 'edit' && this.props.appID){
      console.log('componentWillMount if edit ')
      this.setState({ appID : this.props.appID });
    }
    let currentApp = firebase.database().ref('apps/'+this.state.appID);
    currentApp.once('value').then((app) => {
      let APP = app.val();
      // console.log(app.val());

      this.refs.company_name.value = APP.appData.company_name || '';
      this.refs.company_slogan_name.value = APP.appData.company_slogan_name || '';
      this.refs.company_fantasy_name.value = APP.appData.company_fantasy_name || '';
      this.refs.company_razao_social.value = APP.appData.company_razao_social || '';
      this.refs.company_cnpj.value = APP.appData.company_cnpj || '';
      this.refs.company_ie.value = APP.appData.company_ie || '';
      this.refs.company_address_zipcode.value = APP.appData.company_address_zipcode || '';
      this.refs.company_address.value = APP.appData.company_address || '';
      this.refs.company_address_number.value = APP.appData.company_address_number || '';
      this.refs.company_address_comp.value = APP.appData.company_address_comp || '';
      this.refs.company_address_bairro.value = APP.appData.company_address_bairro || '';
      this.refs.company_address_city.value = APP.appData.company_address_city || '';
      this.refs.company_address_state.value = APP.appData.company_address_state || '';
      this.refs.company_email.value = APP.appData.company_email || '';
      this.refs.company_telephone.value = APP.appData.company_telephone || '';
      this.refs.company_telephone2.value = APP.appData.company_telephone2 || '';
      this.refs.company_telephone_whatsapp.value = APP.appData.company_telephone_whatsapp || '';
      this.refs.company_skype.value = APP.appData.company_skype || '';

      this.refs.company_facebook.value = APP.appData.company_facebook || '';
      this.refs.company_instagram.value = APP.appData.company_instagram || '';
      this.refs.company_youtube.value = APP.appData.company_youtube || '';
      this.refs.company_twitter.value = APP.appData.company_twitter || '';
      this.refs.company_pinterest.value = APP.appData.company_pinterest || '';
      this.refs.company_linkedin.value = APP.appData.company_linkedin || '';
      this.refs.company_blog.value = APP.appData.company_blog || '';

      this.setState({
        company_name : APP.appData.company_name || '',
        company_slogan_name : APP.appData.company_slogan_name || '',
        company_fantasy_name : APP.appData.company_fantasy_name || '',
        company_razao_social : APP.appData.company_razao_social || '',
        company_cnpj : APP.appData.company_cnpj || '',
        company_ie : APP.appData.company_ie || '',
        company_address_zipcode : APP.appData.company_address_zipcode || '',
        company_address : APP.appData.company_address || '',
        company_address_number : APP.appData.company_address_number || '',
        company_address_comp : APP.appData.company_address_comp || '',
        company_address_bairro : APP.appData.company_address_bairro || '',
        company_address_city : APP.appData.company_address_city || '',
        company_address_state : APP.appData.company_address_state || '',
        company_email : APP.appData.company_email || '',
        company_telephone : APP.appData.company_telephone || '',
        company_telephone2 : APP.appData.company_telephone2 || '',
        company_telephone_whatsapp : APP.appData.company_telephone_whatsapp || '',
        company_skype : APP.appData.company_skype || '',

        company_facebook : APP.appData.company_facebook || '',
        company_instagram : APP.appData.company_instagram || '',
        company_youtube : APP.appData.company_youtube || '',
        company_twitter : APP.appData.company_twitter || '',
        company_pinterest : APP.appData.company_pinterest || '',
        company_linkedin : APP.appData.company_linkedin || '',
        company_blog : APP.appData.company_blog || ''
      });

    });
  }

  componentDidMount(){
    console.log('User id -> ',this.state.uid);
    let _self=this;
  };//componentDidMount();

  componentWillReceiveProps(nextProps){
    // console.log('componentWillReceiveProps ',nextProps);
  };//componentWillReceiveProps();

  checkStatusResponse(){
    const closeAlert = (e) => {
      if(e)
        e.preventDefault();
      this.setState({ status_response : null, message_response : null});
    };//closeAlert(e);
    if(this.state.status_response === true){
      setTimeout(() => {
        closeAlert();
      },4000);
      return (<Alert bsStyle={'success'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    if(this.state.status_response === false){
      setTimeout(() => {
        closeAlert();
      },4000);
      if(this.state.status === 'Rascunho'){
        return (<Alert className={'default'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }else if(this.state.status === 'requiredInput'){
        return (<Alert className={'warning'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
      }
      return (<Alert bsStyle={'danger'} onDismiss={closeAlert.bind(this)}>{this.state.message_response}</Alert>);
    }

    return null;
  };//checkStatusResponse();

  checkStates(){
    if(this.state.appID)
      this.setState({ appID : this.state.appID })

    console.log('checkStates exit();');
  };//checkStates();

  resetCurrentState(){
    console.log('resetCurrentState();');
    return this.setState({
      showModal : false,
      confirmDelete : false,
      action : this.props.action,

      uid : this.props.currentUser.uid,
      appID : null,
      name : null,
      slug : null,
      status : true,

      status_response : null,
      message_response : null
    });
  };//resetCurrentState();

  renderModal(){
    let
      pageTitle = this.state.company_name;
    const close = () => {
      this.setState({ showModal : false });
    }
    const confirm = () => {
      firebase.database().ref('apps/'+this.state.appID)
        .remove()
        .then(() => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : false,
            status : 'Deletado',
            status_response : true,
            message_response : '{ ' + this.state.company_name + ' } deletado com sucesso!'
          });
        })
        .catch((err) => {
          this.setState({
            isLoading : false,
            showModal : false,
            confirmDelete : true,
            status : 'Error',
            status_response : false,
            message_response : 'Ocorreu uma falha ao tentar deletar { ' + this.state.company_name + ' } - '+err.message
          });
        });
    }
    return (
      <Modal show={this.state.showModal} onHide={close}>
        <Modal.Header closeButton>
          <Modal.Title>Deseja realmente excluir {pageTitle}?</Modal.Title>
        </Modal.Header>
        <Modal.Footer>
          <Link to={'/dashboard/apps'} onClick={close.bind(this)} className={'btn btn-danger'}>Cancelar</Link>
          <Button onClick={confirm.bind(this)} bsStyle={'success'}>Confirmar</Button>
        </Modal.Footer>
      </Modal>
    );
  };//renderModal();

  /*** FUNCTIONS AUXILIARES DO FORM ***/
  onLoadForm(){
    console.log('onLoadForm();');
  };//onLoadForm(e);
  onSubmitForm(e) {
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const appID = this.refs.appID.value;
      const dataInsert = {
        uid : this.state.uid,
        appID : appID,
        name : this.state.company_name,
        slug : this.state.slug,
        status: true
      };
      firebase.database().ref('apps/'+appID)
        .set(dataInsert, () => {
          this.setState({
            isLoading : false,
            status_response : true,
            message_response : '{ ' + this.state.company_name + ' } gravado com sucesso!'
          });
          console.clear();
          browserHistory.push('/dashboard/apps');
        });
    }
    return false;
  };//onSubmitForm(e);
  onUpdateForm(e){
    e.preventDefault();
    let required = document.getElementsByClassName('required');
    if(required.length > 0){
      this.setState({
        isLoading : true,
        status : 'requiredInput',
        status_response : false,
        message_response : 'Por favor, preencha todos os campos corretamente.'
      });
      return false;
    }else{
      const appID = this.state.appID;
      const dataUpdate = {
        appData: {
          uid : this.state.uid,
          appID : appID,
          slug : slug(this.refs.company_name.value),
          status : this.state.status,

          company_name : this.refs.company_name.value || '',
          company_slogan_name : this.refs.company_slogan_name.value || '',
          company_fantasy_name : this.refs.company_fantasy_name.value || '',
          company_razao_social : this.refs.company_razao_social.value || '',
          company_cnpj : this.refs.company_cnpj.value || '',
          company_ie : this.refs.company_ie.value || '',
          company_address_zipcode : this.refs.company_address_zipcode.value || '',
          company_address : this.refs.company_address.value || '',
          company_address_number : this.refs.company_address_number.value || '',
          company_address_comp : this.refs.company_address_comp.value || '',
          company_address_bairro : this.refs.company_address_bairro.value || '',
          company_address_city : this.refs.company_address_city.value || '',
          company_address_state : this.refs.company_address_state.value || '',
          company_email : this.refs.company_email.value || '',
          company_telephone : this.refs.company_telephone.value || '',
          company_telephone2 : this.refs.company_telephone2.value || '',
          company_telephone_whatsapp : this.refs.company_telephone_whatsapp.value || '',
          company_skype : this.refs.company_skype.value || '',

          company_facebook : this.refs.company_facebook.value || '',
          company_instagram : this.refs.company_instagram.value || '',
          company_youtube : this.refs.company_youtube.value || '',
          company_twitter : this.refs.company_twitter.value || '',
          company_pinterest : this.refs.company_pinterest.value || '',
          company_linkedin : this.refs.company_linkedin.value || '',
          company_blog : this.refs.company_blog.value || ''
        }
      };
      console.log('dataUpdate -> ',dataUpdate);
      firebase.database().ref('apps/'+appID)
        .update(dataUpdate, () => {
          this.setState({
            isLoading : false,
            appID : appID,
            content: '',
            status : 'Publicado',
            status_response : true,
            message_response : '{ ' + this.state.company_name + ' } gravado com sucesso!'
          });
          console.log('Update successful ',this.state.apps)
          // browserHistory.push('/dashboard/apps');
        });
    }
    return false;
  };//onUpdateForm(e);
  onChangeForm(e){
    if(e){
      // console.log('onChangeForm', e.target.value)
      this.setState({
        company_name : this.refs.company_name.value,
        company_slogan_name : this.refs.company_slogan_name.value,
        company_fantasy_name : this.refs.company_fantasy_name.value,
        company_razao_social : this.refs.company_razao_social.value,
        company_cnpj : this.refs.company_cnpj.value,
        company_ie : this.refs.company_ie.value,
        company_address_zipcode : this.refs.company_address_zipcode.value,
        company_address : this.refs.company_address.value,
        company_address_number : this.refs.company_address_number.value,
        company_address_comp : this.refs.company_address_comp.value,
        company_address_bairro : this.refs.company_address_bairro.value,
        company_address_city : this.refs.company_address_city.value,
        company_address_state : this.refs.company_address_state.value,
        company_email : this.refs.company_email.value,
        company_telephone : this.refs.company_telephone.value,
        company_telephone2 : this.refs.company_telephone2.value,
        company_telephone_whatsapp : this.refs.company_telephone_whatsapp.value,
        company_skype : this.refs.company_skype.value
      });
    }
  };//onChangeForm(e);
  _onChangeCompanyName(e){
    if(e){
      this.setState({company_name: e.target.value});
    }
  };//_onChangeCompanyName(e);
  _onChangeSloganCompanyName(e){
    if(e){
      this.setState({company_slogan_name: e.target.value});
    }
  };//_onChangeSloganCompanyName(e);
  _onChangeCompanyZipcode(e){
    if(e){
      let
        cep = e.target.value;

        if(cep.length > 7)
          axios
            .get('https://viacep.com.br/ws/'+cep+'/json/')
            .then( (res) => {
              // console.log(res.data);

              this.refs.company_address_zipcode.value = res.data.cep;
              this.refs.company_address.value = res.data.logradouro;
              this.refs.company_address.disabled = true;
              this.refs.company_address_number.value = '';
              this.refs.company_address_comp.value = res.data.complemento;
              this.refs.company_address_bairro.value = res.data.bairro;
              this.refs.company_address_bairro.disabled = true;
              this.refs.company_address_city.value = res.data.localidade;
              this.refs.company_address_city.disabled = true;
              this.refs.company_address_state.value = res.data.uf;
              this.refs.company_address_state.disabled = true;

              this.setState({
                company_address_zipcode : this.refs.company_address_zipcode.value,
                company_address : this.refs.company_address.value,
                company_address_comp : this.refs.company_address_comp.value,
                company_address_bairro : this.refs.company_address_bairro.value,
                company_address_city : this.refs.company_address_city.value,
                company_address_state : this.refs.company_address_state.value
              });

            }).catch((err) => {
              console.log('error -> ',err)
            });
      console.log('CEP -> ',);
      // this.setState({
      //   company_slogan_name: e.target.value
      // });

    }
  };//_onChangeCompanyZipcode(e);
  _onChangeUserName(e){
    if(e){
      this.setState({user_name: e.target.value});
    }
  };//_onChangeUserName(e);
  onChangeTiny(e){
    if(e)
      this.setState({ content : e.target.getContent() });
  };//onChangeTiny(e);

  changePackageDashboard(e){
    e.preventDefault();
    console.log('target -> ',e.target);
    let
      _self=this,
      packParent = e.target.parentElement.parentElement.parentElement.children[0].innerHTML,
      pack = e.target.dataset.module;
    console.log('changePackageStatus(); -> ',pack);
    console.log('changePackageStatus(); packParent -> ',packParent);

    const appID = _self.state.appID;

    _self.packages = _self.state.packages;
    switch (pack) {
      case 'appManager_status':
        _self.packages.DASHBOARD.appManager = !_self.state.packages.DASHBOARD.appManager;;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/DASHBOARD/appManager')
          .set(_self.state.packages.DASHBOARD.appManager, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'category_status':
        _self.packages.DASHBOARD.category = !_self.state.packages.DASHBOARD.category;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/DASHBOARD/category')
          .set(_self.state.packages.DASHBOARD.category, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'footer_status':
        _self.packages.DASHBOARD.footer = !_self.state.packages.DASHBOARD.footer;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/DASHBOARD/footer')
          .set(_self.state.packages.DASHBOARD.footer, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'logo_status':
        _self.packages.DASHBOARD.logo = !_self.state.packages.DASHBOARD.logo;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/DASHBOARD/logo')
          .set(_self.state.packages.DASHBOARD.logo, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'navHeader_status':
        _self.packages.DASHBOARD.navHeader = !_self.state.packages.DASHBOARD.navHeader;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/DASHBOARD/navHeader')
          .set(_self.state.packages.DASHBOARD.navHeader, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'page_status':
        _self.packages.DASHBOARD.page = !_self.state.packages.DASHBOARD.page;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/DASHBOARD/page')
          .set(_self.state.packages.DASHBOARD.page, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'product_status':
        _self.packages.DASHBOARD.product = !_self.state.packages.DASHBOARD.product;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/DASHBOARD/product')
          .set(_self.state.packages.DASHBOARD.product, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'settings_status':
        _self.packages.DASHBOARD.settings = !_self.state.packages.DASHBOARD.settings;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/DASHBOARD/settings')
          .set(_self.state.packages.DASHBOARD.settings, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      default:
        console.log('Package não encontrado! ',pack);
    }
  }//changePackageDashboard(e);

  changePackageShop(e){
    e.preventDefault();
    console.log('target -> ',e.target);
    let
      _self=this,
      packParent = e.target.parentElement.parentElement.parentElement.children[0].innerHTML,
      pack = e.target.dataset.module;
    console.log('changePackageStatus(); -> ',pack);
    console.log('changePackageStatus(); packParent -> ',packParent);

    const appID = _self.state.appID;

    _self.packages = _self.state.packages;
    switch (pack) {
      case 'cart_status':
        _self.packages.SHOP.cart = !_self.state.packages.SHOP.cart;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/SHOP/cart')
          .set(_self.state.packages.SHOP.cart, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'cartBox_status':
        _self.packages.SHOP.cartBox = !_self.state.packages.SHOP.cartBox;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/SHOP/cartBox')
          .set(_self.state.packages.SHOP.cartBox, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'footer_status':
        _self.packages.SHOP.footer = !_self.state.packages.SHOP.footer;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/SHOP/footer')
          .set(_self.state.packages.SHOP.footer, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'loginBox_status':
        _self.packages.SHOP.loginBox = !_self.state.packages.SHOP.loginBox;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/SHOP/loginBox')
          .set(_self.state.packages.SHOP.loginBox, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'productGrid_status':
        _self.packages.SHOP.productGrid = !_self.state.packages.SHOP.productGrid;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/SHOP/productGrid')
          .set(_self.state.packages.SHOP.productGrid, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'searchBox_status':
        _self.packages.SHOP.searchBox = !_self.state.packages.SHOP.searchBox;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/SHOP/searchBox')
          .set(_self.state.packages.SHOP.searchBox, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      case 'topbarHeader_status':
        _self.packages.SHOP.topbarHeader = !_self.state.packages.SHOP.topbarHeader;
        _self.setState({ packages : _self.packages });

        firebase.database().ref('apps/'+appID+'/packages/SHOP/topbarHeader')
          .set(_self.state.packages.SHOP.topbarHeader, () => {
            _self.setState({
              isLoading : false,
              status_response : true,
              message_response : '{ MODULO: '+pack+' } gravado com sucesso!'
            });
          });
        break;
      default:
        console.log('Package não encontrado! ',pack);
    }
  }//changePackageShop(e);

  changePackageSite(e){
    e.preventDefault();
    console.log('target -> ',e.target);
    let
      _self=this,
      packParent = e.target.parentElement.parentElement.parentElement.children[0].innerHTML,
      // pack = e.target.classList[0];
      pack = e.target.dataset.module;
    console.log('changePackageStatus(); -> ',pack);
    console.log('changePackageStatus(); packParent -> ',packParent);

    // const appID = _self.state.appID;

    _self.packages = _self.state.packages;
    switch (pack) {
      case 'footer_status':
        _self.packages.SITE.footer = !_self.state.packages.SITE.footer;
        _self.setState({ packages : _self.packages });
        break;
      case 'logo_status':
        _self.packages.SITE.logo = !_self.state.packages.SITE.logo;
        _self.setState({ packages : _self.packages });
        break;
      case 'navHeader_status':
        _self.packages.SITE.navHeader = !_self.state.packages.SITE.navHeader;
        _self.setState({ packages : _self.packages });
        break;
      default:
        console.log('Package não encontrado! ',pack);
    }
  }//changePackageSite(e);

  onDropFile(files){
    console.log('Received files: ', files);
    this.setState({files:files});
    this.runUploadFiles(files);
  };//onDropFile(files);

  onDropFileClick(){
    console.log('this.refs.dropzone' , this.refs.dropzone)
    this.refs.dropzone.open();
  };//onDropFileClick();

  runUploadFiles(files){
    console.log('runUploadFiles');
    let
      _self=this,
      storageRef = firebase.storage().ref('apps/'+ this.state.appID +'/');

    if(files.length > 0){
      // eslint-disable-next-line
      files.map((file,key) => {
        if(key === 0){
          // let fileID = TOOLS.uniqueID();
          let fileName_old = file.name,
              // fileName = fileID + '_' + fileName_old,
              fileName = _self.state.appID + '_logo_' + fileName_old,
              fileRef = storageRef.child('uploads/' + fileName),
              // pathFile = fileRef.fullPath,
              // fileSize = file.size,
              // fileType = file.type,
              uploadTask = fileRef.put(file);
              _self.setState({isLoading : true});

          uploadTask.on('state_changed', (snapshot) => {
            console.log('snapshot -> ',snapshot);
          }, (error) => {
            console.log('error -> ',error);
          }, () => {
            fileRef.getDownloadURL().then((url) => {
              _self.setState({ logoURL:url });

              firebase.database().ref('apps/'+_self.state.appID+'/appData/logoURL')
                .set(_self.state.logoURL, () => {
                  _self.setState({
                    isLoading : false,
                    status_response : true,
                    message_response : 'Logo configurada com sucesso!'
                  });
                });

            }).catch((error) => {
              console.log(error);
            });
          });

          console.log('this.state.logoURL -> ', this.state.logoURL);
          console.log('uploadTask -> ', uploadTask);
        }
      });
    }
  };//runUploadFiles();

  tabGeral(){
    return (
      <form onSubmit={this.onUpdateForm}>
        <Col xs={12} md={12}>
          <Col xs={12} md={6} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Nome da Empresa <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                type="text"
                ref="company_name"
                value={APP_CONFIG.PROJECT_NAME}
                disabled
              />
            </div>
          </Col>
          <Col xs={12} md={6} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Slogan da Empresa</label>
              <input
                className={'form-control no-radius'}
                type="text"
                ref="company_slogan_name"
                value={APP_CONFIG.PROJECT_SLOGAN}
                disabled
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Razão Social <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_razao_social"
                placeholder="Razão Social"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Nome Fantasia <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_fantasy_name"
                placeholder="Nome Fantasia"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>CNPJ <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_cnpj"
                placeholder="CNPJ"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>I.E <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_ie"
                placeholder="Insc. Estadual"
              />
            </div>
          </Col>
          <Col xs={12} md={3} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>CEP <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this._onChangeCompanyZipcode}
                type="text"
                ref="company_address_zipcode"
                placeholder="CEP"
              />
            </div>
          </Col>
          <Col xs={12} md={9} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Endereço <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_address"
                placeholder="Endereço"
              />
            </div>
          </Col>
          <Col xs={12} md={3} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Número <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_address_number"
                placeholder="Número"
              />
            </div>
          </Col>
          <Col xs={12} md={3} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Complemento</label>
              <input
                className={'form-control no-radius'}
                onChange={this.onChangeForm}
                type="text"
                ref="company_address_comp"
                placeholder="Complemento"
              />
            </div>
          </Col>
          <Col xs={12} md={3} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Bairro <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_address_bairro"
                placeholder="Bairro"
              />
            </div>
          </Col>
          <Col xs={12} md={3} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Cidade <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_address_city"
                placeholder="Cidade"
              />
            </div>
          </Col>
          <Col xs={12} md={3} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Estado <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_address_state"
                placeholder="Estado"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>E-mail <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_email"
                placeholder="E-mail"
              />
            </div>
          </Col>
          <Col xs={12} md={3} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Telefone</label>
              <input
                className={'form-control no-radius'}
                onChange={this.onChangeForm}
                type="text"
                ref="company_telephone"
                placeholder="Telefone da Loja"
              />
            </div>
          </Col>
          <Col xs={12} md={4} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Telefone de Contato <span className={'required_symbol'}>*</span></label>
              <input
                className={'form-control no-radius'}
                onBlur={TOOLS.requiredInput}
                onChange={this.onChangeForm}
                type="text"
                ref="company_telephone2"
                placeholder="Telefone de Contato"
              />
            </div>
          </Col>
          <Col xs={12} md={4} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Whatsapp</label>
              <input
                className={'form-control no-radius'}
                onChange={this.onChangeForm}
                type="text"
                ref="company_telephone_whatsapp"
                placeholder="Whatsapp da Loja"
              />
            </div>
          </Col>
          <Col xs={12} md={4} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Skype</label>
              <input
                className={'form-control no-radius'}
                onChange={this.onChangeForm}
                type="text"
                ref="company_skype"
                placeholder="Skype"
              />
            </div>
          </Col>
          <Col xs={12} md={4} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div>
              <label>Logo</label>
              <Col xs={12} md={12} className={'no-padding'}>
                {!this.state.logoURL ?
                  <div>
                    <Dropzone ref="dropzone" className={'dropzone'} activeClassName={'dragIn'} onDrop={this.onDropFile} >
                      <span>Arraste sua logo para cá, ou clique para selecionar o arquivo.</span>
                    </Dropzone>
                    <button type="button" onClick={this.onDropFileClick} style={{display:'none'}}>
                      OpenBox
                    </button>
                  </div>
                :
                  <Image src={this.state.logoURL} responsive thumbnail />
                }
              </Col>
            </div>
          </Col>
        </Col>
        <Button
          onClick={this.onUpdateForm}
          className="z-index5 btn-success pull-right no-radius"
          >Salvar
        </Button>
        {/*<Col xs={12} md={2} className={'no-paddingRight pull-right'}>
          <Link
            to={'/dashboard/apps/'}
            className="btn btn-danger pull-right no-radius"
            >Cancelar
          </Link>
        </Col>*/}
      </form>
    );
  }//tabGeral();

  tabSocial(){
    return (
      <form onChange={this.onChangeForm} onSubmit={this.onUpdateForm}>
        <Col xs={12} md={12}>
          <Col xs={12} md={6} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Facebook</label>
              <input
                className={'form-control no-radius'}
                type="text"
                ref="company_facebook"
                placeholder="Facebook URL"
              />
            </div>
          </Col>
          <Col xs={12} md={6} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Instagram</label>
              <input
                className={'form-control no-radius'}
                type="text"
                ref="company_instagram"
                placeholder="Instagram URL"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Youtube</label>
              <input
                className={'form-control no-radius'}
                onChange={this._onChangeUserName}
                type="text"
                ref="company_youtube"
                placeholder="Youtube URL"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Twitter</label>
              <input
                className={'form-control no-radius'}
                onChange={this._onChangeUserName}
                type="text"
                ref="company_twitter"
                placeholder="Twitter URL"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Google +</label>
              <input
                className={'form-control no-radius'}
                onChange={this._onChangeUserName}
                type="text"
                ref="company_googleplus"
                placeholder="Google+ URL"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Pinterest</label>
              <input
                className={'form-control no-radius'}
                onChange={this._onChangeUserName}
                type="text"
                ref="company_pinterest"
                placeholder="Pinterest URL"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Linkedin</label>
              <input
                className={'form-control no-radius'}
                onChange={this._onChangeUserName}
                type="text"
                ref="company_linkedin"
                placeholder="Linkedin URL"
              />
            </div>
          </Col>
          <Col xs={12} md={6} style={{marginTop:10}} className={'no-paddingLeft'}>
            <div className={'input-group'}>
              <label>Blog</label>
              <input
                className={'form-control no-radius'}
                onChange={this._onChangeUserName}
                type="text"
                ref="company_blog"
                placeholder="Blog URL"
              />
            </div>
          </Col>
        </Col>
        <Button
          onClick={this.onUpdateForm}
          className="z-index5 btn-success pull-right no-radius"
          >Salvar
        </Button>
      </form>
    );
  }//tabSocial();

  tabLayout(){
    return (
      <Col xs={12} md={12} className={'no-padding'}>
        <ul className={'col-md-4'}>
          <span className={'label label-info'}>DASHBOARD</span>
            <li>
              Menu Full
              { this.state.packages.DASHBOARD.appManager ?
                <span className={'fa fa-toggle-on'} data-module={'appManager_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'appManager_status'} onClick={this.changePackageShop}></span>
              }
            </li>
        </ul>
        <ul className={'col-md-4'}>
          <span className={'label label-info'}>SHOP</span>
            <li>
              Menu Full
              { this.state.packages.DASHBOARD.appManager ?
                <span className={'fa fa-toggle-on'} data-module={'appManager_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'appManager_status'} onClick={this.changePackageShop}></span>
              }
            </li>
        </ul>
        <ul className={'col-md-4'}>
          <span className={'label label-info'}>SITE</span>
            <li>
              Menu Full
              { this.state.packages.DASHBOARD.appManager ?
                <span className={'fa fa-toggle-on'} data-module={'appManager_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'appManager_status'} onClick={this.changePackageShop}></span>
              }
            </li>
        </ul>
      </Col>
    );
  }//tabLayout();

  tabModules(){
    return (
      <Col xs={12} md={12} className={'no-padding'}>
        <ul className={'col-md-4'}>
          <span className={'label label-info'}>DASHBOARD</span>
            <li>
              AppManager
              { this.state.packages.DASHBOARD.appManager ?
                <span className={'fa fa-toggle-on'} data-module={'appManager_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'appManager_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              Category
              { this.state.packages.DASHBOARD.category ?
                <span className={'fa fa-toggle-on'} data-module={'category_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'category_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              Footer
              { this.state.packages.DASHBOARD.footer ?
                <span className={'fa fa-toggle-on'} data-module={'footer_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'footer_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              Logo
              { this.state.packages.DASHBOARD.logo ?
                <span className={'fa fa-toggle-on'} data-module={'logo_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'logo_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              NavHeader
              { this.state.packages.DASHBOARD.navHeader ?
                <span className={'fa fa-toggle-on'} data-module={'navHeader_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'navHeader_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              Page
              { this.state.packages.DASHBOARD.page ?
                <span className={'fa fa-toggle-on'} data-module={'page_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'page_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              Product
              { this.state.packages.DASHBOARD.product ?
                <span className={'fa fa-toggle-on'} data-module={'product_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'product_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              Settings
              { this.state.packages.DASHBOARD.settings ?
                <span className={'fa fa-toggle-on'} data-module={'settings_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'settings_status'} onClick={this.changePackageShop}></span>
              }
            </li>
        </ul>

        <ul className={'col-md-4'}>
          <span className={'label label-info'}>SHOP</span>
            <li>
              Cart
              { this.state.packages.SHOP.cart ?
                <span className={'fa fa-toggle-on'} data-module={'cart_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'cart_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              CartBox
              { this.state.packages.SHOP.cartBox ?
                <span className={'fa fa-toggle-on'} data-module={'cartBox_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'cartBox_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              Footer
              { this.state.packages.SHOP.footer ?
                <span className={'fa fa-toggle-on'} data-module={'footer_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'footer_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              LoginBox
              { this.state.packages.SHOP.loginBox ?
                <span className={'fa fa-toggle-on'} data-module={'loginBox_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'loginBox_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              ProductGrid
              { this.state.packages.SHOP.productGrid ?
                <span className={'fa fa-toggle-on'} data-module={'productGrid_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'productGrid_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              SearchBox
              { this.state.packages.SHOP.searchBox ?
                <span className={'fa fa-toggle-on'} data-module={'searchBox_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'searchBox_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              TopbarHeader
              { this.state.packages.SHOP.topbarHeader ?
                <span className={'fa fa-toggle-on'} data-module={'topbarHeader_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'topbarHeader_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              NavFull
              { this.state.packages.SHOP.navFull ?
                <span className={'fa fa-toggle-on'} data-module={'navFull_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'navFull_status'} onClick={this.changePackageShop}></span>
              }
            </li>
        </ul>

        <ul className={'col-md-4'}>
          <span className={'label label-info'}>SITE</span>
            <li>
              Cart
              { this.state.packages.SITE.cart ?
                <span className={'fa fa-toggle-on'} data-module={'cart_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'cart_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              CartBox
              { this.state.packages.SITE.CartBox ?
                <span className={'fa fa-toggle-on'} data-module={'CartBox_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'CartBox_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              Footer
              { this.state.packages.SITE.footer ?
                <span className={'fa fa-toggle-on'} data-module={'footer_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'footer_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              LoginBox
              { this.state.packages.SITE.loginBox ?
                <span className={'fa fa-toggle-on'} data-module={'loginBox_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'loginBox_status'} onClick={this.changePackageShop}></span>
              }
            </li>

            <li>
              ProductGrid
              { this.state.packages.SITE.productGrid ?
                <span className={'fa fa-toggle-on'} data-module={'productGrid_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'productGrid_status'} onClick={this.changePackageShop}></span>
              }
            </li>
            <li>
              NavFull
              { this.state.packages.SITE.navFull ?
                <span className={'fa fa-toggle-on'} data-module={'navFull_status'} onClick={this.changePackageShop}></span>
              :
                <span className={'fa fa-toggle-off'} data-module={'navFull_status'} onClick={this.changePackageShop}></span>
              }
            </li>
        </ul>
      </Col>
    );
  }//tabModules();

  tabExtras(){
    return (
      <Col xs={12} md={12} className={'no-padding'}>
        <h1>Extras</h1>
      </Col>
    );
  }//tabExtras();



  /*
   *	 RENDER FUNCTIONS - ACTIONS
   *
   *  Funções que renderizam a view do component,
   *  de acordo a ACTION de this.props.action;
   *  Todas as "ACTIONS Functions" definem document.name:
   *  Ex: document.name = APP_CONFIG.PROJECT_NAME + ' | Page Name';
   *  { actionNull - actionNew - actionEdit - actionDelete }
   */
  actionNull(){
    /*
     *  Responsável por renderizar a table de Páginas
     *  if( this.state.apps.length > 0 )
     */
    document.name = APP_CONFIG.PROJECT_NAME + ' | Configurações';
    if( !this.state.isLoading ){
      return (
        <div>
          <Col xs={12} md={12} className={'no-padding'}>
            <Tabs defaultActiveKey={1} className={'tabs'} id={'settingsTab'}>
              <Tab eventKey={1} title={'Geral'}>
                {this.tabGeral()}
              </Tab>
              <Tab eventKey={2} title={'Modulos'}>
                {this.tabModules()}
              </Tab>
              <Tab eventKey={3} title={'Layout'}>
                {this.tabLayout()}
              </Tab>
              <Tab eventKey={4} title={'Extras'}>
                {this.tabExtras()}
              </Tab>
            </Tabs>
          </Col>
        </div>
      );
    }else{
      return (
        <div>
          <Col xs={12} md={12} className={'no-padding'}>
            <Tabs defaultActiveKey={1} className={'tabs'} id={'settingsTab'}>
              <Tab eventKey={1} title={'Geral'}>
                {this.tabGeral()}
              </Tab>
              <Tab eventKey={2} title={'Redes Sociais'}>
                {this.tabSocial()}
              </Tab>
              <Tab eventKey={3} title={'Modulos'}>
                {this.tabModules()}
              </Tab>
              <Tab eventKey={4} title={'SEO'}>
                {this.tabModules()}
              </Tab>
              <Tab eventKey={5} title={'Layout'}>
                {this.tabLayout()}
              </Tab>
              <Tab eventKey={6} title={'Extras'}>
                {this.tabExtras()}
              </Tab>
            </Tabs>
          </Col>
        </div>
      );
    }
  };//actionNull();

  renderAction(){
    /*
     *  Responsável por verificar e chamar ACTIONS.
     *  Ex: if( this.props.action === 'new' ) this.actionNew();
     */
    if( !this.props.action )
      return (<div>{this.actionNull()}</div>);
  };//renderAction();

  render(){
    return (
      <div>
        {this.checkStatusResponse()}
        {
          this.renderAction()
        }
      </div>
    );
  }
}
